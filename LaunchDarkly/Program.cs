﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LaunchDarkly.Client;

namespace LaunchDarkly
{
    class Program
    {
        static void Main(string[] args)
        {
            LdClient client = new LdClient("sdk-375bed34-3786-40d4-be48-58eb4c8a3913");

            User user = User.WithKey("bobs@example.com")
              .AndFirstName("Bob")
              .AndLastName("Loblaw")
              .AndCustomAttribute("groups", "beta_testers");


            bool value = client.BoolVariation("automate-renewals", user, false);
            if (value)
            {
                Console.WriteLine("Showing feature for user " + user.Key);
            }
            else
            {
                Console.WriteLine("Not showing feature for user " + user.Key);
            }

            client.Flush();

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}